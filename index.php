
<?php
session_start();

if (!(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || 
   $_SERVER['HTTPS'] == 1) ||  
   isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&   
   $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'))
{
   $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
   header('HTTP/1.1 301 Moved Permanently');
   header('Location: ' . $redirect);
   exit();
}

include("simple-php-captcha.php");
$_SESSION['captcha'] = simple_php_captcha();
$reward=0.1;
$reward_with_fees=0.2;

require_once('library.php');
include 'conn.php';
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="./css/bootstrap.min.css">
<link rel="stylesheet" href="style.css">
<script src="./js/jquery-3.4.0.min.js"></script>
<script src="./js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
  <div class="row">
      <div class="col-lg-12">
        <!--<div class='alert alert-info' role='alert'>Update. Checkout <a href="https://gitlab.com/yas3/nerva-games">Source Code for Dice Game for Nerva. </a></div>
        <div class='alert alert-info' role='alert'>Update. Checkout <a href="https://store.yaslabs.com/games">Dice Game for Nerva (Mainnet). </a></div>-->

        <h2 class="py-1 text-center">Nerva Faucet</h2>
        <h3><?php echo $reward;?> XNV Reward</h3>
        <h4>Timeout 60 seconds Globally</h4>
      </div>

    </div>  
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
<?php

$nerva_daemon = new Nerva_Library('localhost', '43931');

$wallet_amount=$nerva_daemon->getbalance();
if (!isset($wallet_amount)) {
?>
    <li class="list-group-item d-flex justify-content-between">
          <span>Balance</span>
          <strong>Not Avaliable</strong>
    </li>
    <li class="list-group-item d-flex justify-content-between">
          <span>Unlocked balance</span>
          <strong>Not Avaliable</strong>
    </li>
    <?php
    //echo "</br>Your balance is: Not Avaliable </br>";
    //echo "Unlocked balance: Not Avaliable";
}else{
    $real_wallet_amount = $wallet_amount['balance'] / 1000000000000;
    $real_amount_rounded = round($real_wallet_amount, 6);

    $unlocked_wallet_amount = $wallet_amount['unlocked_balance'] / 1000000000000;
    $unlocked_amount_rounded = round($unlocked_wallet_amount, 6);

    $res=$nerva_daemon->get_transfers("out",true);
    //$nerva_daemon->_print($res);
    $total=0;
    $other_out=0;
    $final_height=0;

    foreach($res["out"] as $value){
    // $nerva_daemon->_print($value);
        if($value['height']>$final_height){
          $final_height=$value['height'];
        }
      
        if($value['height']>524567 && $value['amount']==0.1*1000000000000){
        $total+=$value['amount'];
        }elseif($value['height']>524567){
          $other_out+=$value['amount'];
        }  
       
    }
    //finding last used height in pool also
    $res=$nerva_daemon->get_transfers("pool",true);
    foreach($res["pool"] as $value){
      // $nerva_daemon->_print($value);
          if($value['height']>$final_height){
            $final_height=$value['height'];
          }    
    }

     //finding last used height in pending also
     $res=$nerva_daemon->get_transfers("pending",true);
     foreach($res["pending"] as $value){
       // $nerva_daemon->_print($value);
           if($value['height']>$final_height){
             $final_height=$value['height'];
           }    
     }

    $total_round=round($total/1000000000000, 6);

    $res=$nerva_daemon->get_transfers("in",true);
   
    $total_in=0;
    $other_in=0;
    foreach($res["in"] as $value){
        if($value['height']>524567 && $value['payment_id']=="0000000000000000"){
          $total_in+=$value['amount'];
        }elseif($value['height']>524567){
          $other_in+=$value['amount'];
        }  
    }

    $current_height=$nerva_daemon->getheight();
    $total_in_round=round($total_in/1000000000000, 6);
    $other_in_round=round($other_in/1000000000000, 6);
    $other_in_round = number_format($other_in_round,0,'.','');
    $other_out_round=round($other_out/1000000000000, 6);
    $other_out_round = number_format($other_out_round,0,'.','');
?>
    <li class="list-group-item d-flex justify-content-between">
          <span>Balance</span>
          <strong><?php echo $real_amount_rounded;?> XNV</strong>
    </li>
    <li class="list-group-item d-flex justify-content-between">
          <span>Unlocked balance</span>
          <strong><?php echo $unlocked_amount_rounded;?> XNV</strong>
    </li>
    <li class="list-group-item d-flex justify-content-between">
    <span>Total Rewarded Out</span>
    <strong><?php echo $total_round;?> XNV</strong>
    </li>
    <li class="list-group-item d-flex justify-content-between">
    <span>Total Donated In</span>
    <strong><?php echo $total_in_round;?> XNV</strong>
    </li>
    <li class="list-group-item d-flex justify-content-between">
    <span>Total Game In</span>
    <strong><?php echo $other_in_round;?> XNV</strong>
    </li>
    <li class="list-group-item d-flex justify-content-between">
    <span>Total Game Out</span>
    <strong><?php echo $other_out_round;?> XNV</strong>
    </li>
    <?php
    //echo "Your balance is: " . $real_amount_rounded . " XNV </br>";
   // echo "Unlocked balance: " . $unlocked_amount_rounded . " XNV </br>";
}
$don_add=$nerva_daemon->address();
?>
</div>
<div class="col-lg-4 col-md-4 col-sm-6">
      <strong class=".text-dark">Node info</strong>
      <ul class=".text-dark">
        <li>Status: <span id="node_connection" class="text-danger">Offline</span></li>
        <li>Version: <span id="node_ver">...</span></li>
        <li>Height: <span id="node_height">...</span></li>
      </ul>

    </div>

</div>


<div class="p-2 mb-2 rounded">
        <h4 class="font-italic">Donation Address to keep the faucet flowing (Send less than 2 XNV)</h4>
        <p class="mb-0 word-wrap"><?php echo $don_add['address'];?></p>
</div>


<?php
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}



?>
<div class="row">
    <div class="col-lg-12 mb-2">
    
    <?php

$sql = "SELECT * FROM table1 where node=1";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $date1 = strtotime("now");
    $datetime2 = strtotime($row['time']);
    $interval = $date1-$datetime2;
    $interval1=$date1-$last_time;
}
$interval_disp="";
if($interval>60*60){
  $interval_disp=round($interval/(60*60),1) ." hr";
}else if($interval>60){
  $interval_disp=round($interval/60,1) ." min";
}else{
  $interval_disp=$interval ." sec";
}
$diff_height=$current_height['height']-$final_height;
echo "<br><span>Since last drop ".$interval_disp." </span><br>";

if($interval>60){
  if($unlocked_wallet_amount>=$reward_with_fees){
    echo "<br><div class='alert alert-success' role='alert'><strong>You have an opportunity</strong> to get Nerva now.</div>";
  }else{
    echo "<br><div class='alert alert-danger' role='alert'>You need to wait untill unlocked balance to get more than ".$reward_with_fees." XNV</div>";

  }
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $address = test_input($_POST["address"]);
        $code = test_input($_POST["code"]);

        if($code!=$_SESSION['save_code'] || $code==""){
            echo "<br><div class='alert alert-danger' role='alert'><strong>Wrong Captcha</strong>, You are a robot.</div><br>";
        }else{
            if($unlocked_wallet_amount>=$reward_with_fees){
              $nerva_daemon->transfer($reward, $address);
            }
            $sql="update table1 set time=CURRENT_TIMESTAMP WHERE node=1";
            $result = $conn->query($sql);  
            if($unlocked_wallet_amount>=$reward_with_fees){
                echo "<div class='alert alert-success' role='alert'><strong>Transfer complete.</strong></div>";
                header( "refresh:2;url=https://store.yaslabs.com/faucet" );
            }else{
                echo "<div class='alert alert-danger' role='alert'><strong>Unlocked balance is less than ".$reward_with_fees."</strong></div>";

            }
        }
      
    }
}else{
    $rem=60-$interval;
    echo "<div class='alert alert-danger' role='alert'><strong>You need to wait untill drops collect.</strong> Refresh after ".$rem." seconds</div>";
}
$conn->close();
$_SESSION['save_code']=$_SESSION['captcha']['code'];
?>
<br>
<img src="drop-new.png" width="100" height="120"><hr>
<form action="#" method="post">

  <label>Nerva(XNV) Address </label>
  <input class="form-control mr-sm-2" type="text" name="address" value="" maxlength="130" size="130"><br>
  <img src="<?php echo $_SESSION['captcha']['image_src'];?>"><br>
  <label>Verify captcha code to prove you are not a robot.</label>
  <input class="form-control mr-sm-2" type="text" name="code" value="" maxlength="10" size="10"><br>
  <input class="btn btn-primary" type="submit" value="Get Nerva">
</form>


      </div>
    </div>

<div class="row">
  <div class="card-deck mb-3 text-center">
    <div class="col-sm-12 col-md-6 col-lg-3  mb-2">
      <div class="card mb-4 shadow-sm">
        <div class="card-header">
          <h4 class="my-0 font-weight-normal">Mining Nerva</h4>
        </div>
        <div class="card-body">
          
          <ul class="list-unstyled mt-3 mb-4">
          Solo CPU mining means everyone with a computer has a chance to mine blocks.
          </ul>
          <a href="https://getnerva.org/#downloads"><button type="button" class="btn btn-lg btn-block btn-outline-primary">Start mining</button></a>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12 mb-2">

      <div class="card mb-4 shadow-sm">
        <div class="card-header">
          <h4 class="my-0 font-weight-normal">Calculator</h4>
        </div>
        <div class="card-body">
          
          <ul class="list-unstyled mt-3 mb-4">
              Mining Calculator
          </ul>
          <a href="https://webapp.yaslabs.com/"> <button type="button" class="btn btn-lg btn-block btn-outline-primary">Calculate</button></a>
        </div>
      </div>

    </div>
    <div class="col-lg-3 col-md-6 col-sm-12 mb-2">

      <div class="card mb-4 shadow-sm">
        <div class="card-header">
          <h4 class="my-0 font-weight-normal">Shop with Nerva</h4>
        </div>

        <div class="card-body">
          
          <ul class="list-unstyled mt-3 mb-4">
              After receiving your drop of nerva you can now spend it here
          </ul>
          <a href="https://store.yaslabs.com"> <button type="button" class="btn btn-lg btn-block btn-outline-primary">Shop</button></a>
        </div>
      </div>

    </div>
    <div class="col-lg-3 col-md-6 col-sm-12 mb-2">

      <div class="card mb-4 shadow-sm">
        <div class="card-header">
          <h4 class="my-0 font-weight-normal">Statistics</h4>
        </div>
        <div class="card-body">
          
          <ul class="list-unstyled mt-3 mb-4">
            Get latest news about nerva and more.
          </ul>
          <a href="https://freeboard.io/board/EV5-se"> <button type="button" class="btn btn-lg btn-block btn-outline-primary">Statistics</button></a>

          
        </div>
      </div>
    </div>
  </div>
  <div class="row">

    
  </div>
   
        
</div>

<script>
  refreshDelay=30000;
  function fetchLiveStats() {
        $.ajax({
            url: "https://store.yaslabs.com/explorer/api/getinfo.php",
            dataType: 'json',
            cache: 'false'
        }).done(function(data, success){
            //pulseLiveUpdate();
            lastStats = data.result;
			      nodeStatus = success;
            //currentPage.update();
			nodeInfo();
        }).always(function () {
			setTimeout(function() {
				fetchLiveStats();
			}, refreshDelay);
        });
  }

  function nodeInfo() {

    if(nodeStatus) {
			$('#node_connection').html('Online').addClass('.text-dark').removeClass('text-danger');
      $('#node_height').html(parseInt(lastStats['height']));
      
			if (lastStats['version'] !== 'undefined'){
				$('#node_ver').html(lastStats['version']);
			}
		} else {
			$('#node_connection').html('Offline').addClass('text-danger').removeClass('text-success');
		}
  }
  fetchLiveStats();
   
  </script>

</body>
</html>
